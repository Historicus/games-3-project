// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "DaviesBullet.h"
#include "SmokeparticleManager.h"


//=============================================================================
// default constructor
//=============================================================================
Bullet::Bullet() : Entity()
{
    spriteData.width = BulletNS::WIDTH;           // size of Ship1
    spriteData.height = BulletNS::HEIGHT;
    spriteData.rect.bottom = BulletNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = BulletNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    frameDelay = BulletNS::BULLET_ANIMATION_DELAY;


	startFrame = BulletNS::PLAYER_BULLET_IDLE_START;     // first frame of ship animation
	endFrame     = BulletNS::PLAYER_BULLET_IDLE_END;     // last frame of ship animation
	
	currentFrame = startFrame;
	radius = 17;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	//TODO: use this for damage
	//dead = false;
	mass = BulletNS::MASS;
	collisionType = entityNS::CIRCLE;
	health = 5;
	//deadBullet = false;
	fired = false;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool Bullet::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Bullet::draw()
{
    Image::draw(); 

}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Bullet::update(float frameTime)
{
	Entity::update(frameTime);
	//spriteData.angle += frameTime * BulletNS::ROTATION_RATE;  // rotate the ship
	spriteData.x += frameTime * velocity.x;     // move ship along X 
	spriteData.y += frameTime * velocity.y;     // move ship along Y
}


