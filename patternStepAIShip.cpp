#include "patternStepAIShip.h"

PatternStep::PatternStep()
{
    active = false;                 
	timeInStep = 0;
	entity = NULL;  
	timeForStep = 0;
	action = SEND;
}

void PatternStep::initialize(AIShip *e)
{
	entity = e;
}

void PatternStep::update(float frameTime)
{
	if (!active) return;
	timeInStep += frameTime;
	if (timeInStep > timeForStep)
	{
		timeInStep = 0;
		active = false;
	}
	switch (action)
	{
	case SEND:
		entity->setX(0);
		break;
	case TRACK:
		entity->vectorTrack();
		break;
	case BLIND:
		entity->blindShooting(frameTime);
		break;
	}
}