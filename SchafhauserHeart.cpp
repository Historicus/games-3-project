// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "Schafhauserheart.h"

//=============================================================================
// default constructor
//=============================================================================
heart::heart() : Entity()
{
    spriteData.width = heartNS::WIDTH;           // size of Ship1
    spriteData.height = heartNS::HEIGHT;
    spriteData.rect.bottom = heartNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = heartNS::WIDTH;
    frameDelay = heartNS::HEART_ANIMATION_DELAY;
    startFrame = heartNS::HEART3_START;     // first frame of ship animation
    endFrame     = heartNS::HEART3_END;     // last frame of ship animation
    currentFrame = startFrame;
    radius = heartNS::WIDTH/2.0;

	collisionType = entityNS::CIRCLE;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool heart::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void heart::draw()
{
    Image::draw();              // draw ship
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void heart::update(float frameTime)
{
    Entity::update(frameTime);
	
	//=======================================================================
	//								MOVEMENT
	//=======================================================================
	int directionX = 0;
	int directionY = 0;

	if(input->isKeyDown(PLAYER_RIGHT_KEY))            // if move right
	{
		directionX++;
	}
	if(input->isKeyDown(PLAYER_LEFT_KEY))             // if move left
	{
		directionX--;
	}
	
	float tempX = spriteData.x + directionX * frameTime * heartNS::SPEED;
	float tempY = spriteData.y + directionY * frameTime * heartNS::SPEED;

	if ((tempX + spriteData.width*heartNS::HEART_IMAGE_SCALE) >= GAME_WIDTH)
		directionX = 0;
	if ((tempX <= 0))
		directionX = 0;
	if ((tempY + spriteData.height*heartNS::HEART_IMAGE_SCALE) >= GAME_HEIGHT-97 || tempY <= 89)
		directionY = 0;
	spriteData.x = spriteData.x + directionX * frameTime * heartNS::SPEED;
	spriteData.y = spriteData.y + directionY * frameTime * heartNS::SPEED;

}


void heart::reset(){
	this->setVisible(true); 
	this->setActive(true);
	this->setX(GAME_WIDTH/2-50);                    // start above and left of planet
	this->setY(GAME_HEIGHT - (heartNS::HEIGHT + 5));
}