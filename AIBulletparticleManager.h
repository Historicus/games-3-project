#ifndef _AI_BULLET_PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _PLAYER_BULLET_PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "Bulletparticle.h"
#include "constants.h"
#include "textureManager.h"

class AIBulletParticleManager
{
	private:
	BulletParticle particles[MAX_NUMBER_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float AIBulletParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	AIBulletParticleManager();
	~AIBulletParticleManager();
	void AIBulletParticleManager::setInvisibleAllParticles();
	void AIBulletParticleManager::setVisibleNParticles(int n);
	void AIBulletParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void AIBulletParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool AIBulletParticleManager::initialize(Graphics *g);

	void AIBulletParticleManager::update(float frametime);
	void AIBulletParticleManager::draw();


};







#endif _PLAYER_BULLET_PARTICLE_MANAGER_H