
#include "menu.h"
#include "collisionTypes.h" //Nick added this. in order to talk to gameStates in collisionTypes.cpp

Menu::Menu() 
{
	selectedItem = -1;	//nothing return
	menuItemFont = new TextDX();
	menuHeadingFont = new TextDX();

	submenuItemFontHighlight =  new TextDX();
	submenuHeadingFont = new TextDX();

	playGame = false;
	infiniteLives = false;
	skipLevel = false;
}

void Menu::initialize(Graphics *g, Input *i, Game *gameptr)
{
	
	audio = gameptr->getAudio();

	currentMenu = "mainMenu";

	
	mainMenu.push_back("Star Destroyer VII");
	mainMenu.push_back("Play");
	mainMenu.push_back("Cheat Codes");
	mainMenu.push_back("Controls");

	cheatCodesMenu.push_back("Cheat Codes");
	cheatCodesMenu.push_back("Infinite lives");
	cheatCodesMenu.push_back("Skip the 1st level");
	cheatCodesMenu.push_back("Go Back");

	lifeStoryMenu.push_back("Life Story");
	lifeStoryMenu.push_back("Sam Kenney");
	lifeStoryMenu.push_back("Nick Schafhauser");
	lifeStoryMenu.push_back("Clint Davies");
	lifeStoryMenu.push_back("Go Back");


	highlightColor = graphicsNS::BLUE;
	normalColor = graphicsNS::WHITE;
	menuAnchor = D3DXVECTOR2(175,415);
	input = i;
	verticalOffset = 40;
	linePtr = 0;
	sublinePtr = 0;
	selectedItem = -1;
	subselectedItem = -1;
	graphics = g;
	menuItemFont = new TextDX();
	menuHeadingFont = new TextDX();
	menuItemFontHighlight = new TextDX();

	submenuItemFont = new TextDX();
	submenuItemFontHighlight =  new TextDX();
	submenuHeadingFont = new TextDX();

	if(menuItemFont->initialize(graphics, 40, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if(menuItemFontHighlight->initialize(graphics, 40, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if(menuHeadingFont->initialize(graphics, 40, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuHeading font"));

	if(submenuItemFont->initialize(graphics, 40, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if(submenuItemFontHighlight->initialize(graphics, 40, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if(submenuHeadingFont->initialize(graphics, 100, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuHeading font"));

	menuHeadingFont->setFontColor(normalColor);
	menuItemFont->setFontColor(normalColor);
	menuItemFontHighlight->setFontColor(highlightColor);

	submenuHeadingFont->setFontColor(normalColor);
	submenuItemFont->setFontColor(normalColor);
	submenuItemFontHighlight->setFontColor(highlightColor);

	upDepressedLastFrame = false;
	downDepressedLastFrame = false;
	controls = false;
	
	if (!checkTM.initialize(graphics,CHECK_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing check texture"));

    if (!checkTexture.initialize(graphics,0,0,0,&checkTM))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing check"));


		if (!checkTM2.initialize(graphics,CHECK_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing check texture"));

    if (!checkTexture2.initialize(graphics,0,0,0,&checkTM2))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing check"));


	if (!ControllsScreenTM.initialize(graphics,CONTROLLS_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing check"));
	 if (!ControllsScreen.initialize(graphics,0,0,0,&ControllsScreenTM))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing check"));
}

void Menu::update()
{

	if (input->wasKeyPressed('W'))
	{
		linePtr--;
		audio->playCue(SELECT);
	}
	if (input->wasKeyPressed('S'))
	{
		linePtr++;
		audio->playCue(SELECT);
	}

	if (currentMenu == "lifeStoryMenu"){ //we have 4 options instead of 3
		if (linePtr > 3)
			linePtr = 0;
	}
	else {
		if (linePtr > 2) linePtr = 0;
	}

	if (linePtr < 0) linePtr = 2;

	if (input->wasKeyPressed(VK_RETURN))
		selectedItem = linePtr;
	else selectedItem = -1;

	if ((selectedItem == 2) && (currentMenu == "mainMenu")){ //user chose the life story menu
		currentMenu = "lifeStoryMenu";
	}



	if ((selectedItem == 0) && (currentMenu == "mainMenu")){
		playGame = true; //start the game
		audio->playCue(PLAYGAME);
		//currentMenu = "optionsMenu";
	}
	else if ((selectedItem == 1) && (currentMenu == "mainMenu")){
		currentMenu = "cheatCodesMenu";
	}
	else if ((currentMenu == "optionsMenu") && (selectedItem == 2)){ //then we are hitting exit button within the options menu
		//MessageBeep((UINT) -1);
		//PostQuitMessage(0);
		currentMenu = "mainMenu";
	}
	else if ((currentMenu == "cheatCodesMenu") && (selectedItem == 2)){ //then we are hitting exit button within the Sound Fx  menu
		currentMenu = "mainMenu";
	}
	//else if ((currentMenu == "lifeStoryMenu") && (selectedItem == 3)){ // go back while in life story menu
	//	currentMenu = "mainMenu";
	//}
	else if ((currentMenu == "lifeStoryMenu") && (input->wasKeyPressed(VK_SPACE))){ // go back while in life story menu
		currentMenu = "mainMenu";
		ControllsScreen.setVisible(false);
		controls = false;
		audio->playCue(SELECT);
	}
	else if ((currentMenu == "cheatCodesMenu") && (selectedItem == 0)){ //selected infinite lifes cheat code. make color stay green
		if (infiniteLives){
			infiniteLives = false;
			audio->playCue(INFINITELIVESDISABLED);
		}
		else{
			infiniteLives = true;
			audio->playCue(INFINITELIVES);
		}
	}
	else if ((currentMenu == "cheatCodesMenu") && (selectedItem == 1)){ //selected skip level cheat code. make color stay green
		if (skipLevel){
			skipLevel = false;
			audio->playCue(LEVELSKIPDISABLED);
		}
		else{
			skipLevel = true;
			audio->playCue(LEVELSKIPENABLED);
		}
	}
}

void Menu::displayMenu()
{
	std::string heading = "";
	std::string item1 = "";
	std::string item2 = "";
	std::string item3 = "";
	std::string item4 = "";

	if(currentMenu == "cheatCodesMenu"){
		/*highlightColor = graphicsNS::GREEN;
		menuItemFontHighlight->setFontColor(highlightColor);*/
		heading = cheatCodesMenu[0];
		item1 = cheatCodesMenu[1];
		item2 = cheatCodesMenu[2];
		item3 = cheatCodesMenu[3];
		int foo = 2 * verticalOffset;
		if (linePtr==0)
			menuItemFontHighlight->print(item1, menuAnchor.x-85, menuAnchor.y+foo);
		else
			menuItemFont->print(item1, menuAnchor.x-85, menuAnchor.y+foo);
		foo = 3*verticalOffset;
		if (linePtr==1)
			menuItemFontHighlight->print(item2, menuAnchor.x-125, menuAnchor.y+foo);
		else
			menuItemFont->print(item2, menuAnchor.x-125, menuAnchor.y+foo);
		foo = 4*verticalOffset;
		if (linePtr==2)
			menuItemFontHighlight->print(item3, menuAnchor.x, menuAnchor.y+foo);
		else
			menuItemFont->print(item3, menuAnchor.x, menuAnchor.y+foo);

	}
	else if (currentMenu == "mainMenu"){
		heading = mainMenu[0];
		item1 = mainMenu[1];
		item2 = mainMenu[2];
		item3 = mainMenu[3];
		int foo = 2 * verticalOffset;
		if (linePtr==0)
			menuItemFontHighlight->print(item1, menuAnchor.x+30, menuAnchor.y+foo);
		else
			menuItemFont->print(item1, menuAnchor.x+30, menuAnchor.y+foo);
		foo = 3*verticalOffset;
		if (linePtr==1)
			menuItemFontHighlight->print(item2, menuAnchor.x-55, menuAnchor.y+foo);
		else
			menuItemFont->print(item2, menuAnchor.x-55, menuAnchor.y+foo);
		foo = 4*verticalOffset;
		if (linePtr==2)
			menuItemFontHighlight->print(item3, menuAnchor.x-15, menuAnchor.y+foo);
		else
			menuItemFont->print(item3, menuAnchor.x-15, menuAnchor.y+foo);

	}
	else if (currentMenu == "lifeStoryMenu"){

		controls = true;
		ControllsScreen.setVisible(true);
		ControllsScreen.setX(40);
		ControllsScreen.setY(40);
		ControllsScreen.draw();
	}

	if (currentMenu == "cheatCodesMenu"){
		if (infiniteLives){ //if the user selected infinite lives, make the font stay green
			checkTexture.setX(450);
			checkTexture.setY(507);
			checkTexture.draw();
		}
		if (skipLevel){ //if the user selected skip level, make the font stay green
			checkTexture2.setX(505);
			checkTexture2.setY(545);
			checkTexture2.draw();
		}
	}
}