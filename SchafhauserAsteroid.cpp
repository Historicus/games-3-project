// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "SchafhauserAsteroid.h"

//=============================================================================
// default constructor
//=============================================================================
Asteroid::Asteroid() : Entity()
{
    spriteData.width = AsteroidNS::WIDTH;           // size of Ship1
    spriteData.height = AsteroidNS::HEIGHT;
    spriteData.rect.bottom = AsteroidNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = AsteroidNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    frameDelay = AsteroidNS::ASTEROID_ANIMATION_DELAY;
    startFrame = AsteroidNS::ASTEROID_IDLE_START;     // first frame of ship animation
	endFrame     = AsteroidNS::ASTEROID_IDLE_END;     // last frame of ship animation
	currentFrame = startFrame;
	radius = 17;
	//TODO: use this for damage
	//dead = false;
	mass = AsteroidNS::MASS;
	collisionType = entityNS::CIRCLE;
	health = 5;
	//asteroidDead = false;
	asteroidDead = false;
	timeInState = 0.0f;
	sent = false;
	shaker = left;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool Asteroid::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    asteroidDestroyed.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
    asteroidDestroyed.setFrames(AsteroidNS::ENEMY_DESTROYED_START, AsteroidNS::ENEMY_DESTROYED_END);
    asteroidDestroyed.setCurrentFrame(AsteroidNS::ENEMY_DESTROYED_START);
    asteroidDestroyed.setFrameDelay(AsteroidNS::ENEMY_ANIMATION_DELAY);
	asteroidDestroyed.setLoop(false);
	asteroidDestroyed.setDegrees(0.0f);

    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Asteroid::draw()
{
    Image::draw();              // draw ship
	//TODO: use this for damage
	if (asteroidDead)
		asteroidDestroyed.draw(spriteData, 0);
	
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Asteroid::update(float frameTime)
{
	Entity::update(frameTime);

	if (!asteroidDead)
		spriteData.angle += frameTime * AsteroidNS::ROTATION_RATE;  // rotate the ship
	else {
		spriteData.angle = 135;
	}


	spriteData.x += frameTime * velocity.x;     // move ship along X 
	spriteData.y += frameTime * velocity.y;     // move ship along Y
	// Bounce off walls
	// if hit right screen edge
	if (spriteData.x > GAME_WIDTH-AsteroidNS::WIDTH*getScale())
	{
		// position at right screen edge
		spriteData.x = GAME_WIDTH-AsteroidNS::WIDTH*getScale();
		velocity.x = -velocity.x;               // reverse X direction
		//audio->playCue(MONSTER_BOUNCE);                  // play sound
	} 
	else if (spriteData.x <= 0)                  // else if hit left screen edge
	{
		spriteData.x = 0;                       // position at left screen edge
		velocity.x = -velocity.x;               // reverse X direction
		//audio->playCue(MONSTER_BOUNCE);                  // play sound
	}

	if(asteroidDead)
	{
		asteroidDestroyed.update(frameTime);
		if(asteroidDestroyed.getAnimationComplete())
		{
			spriteData.angle = 135;
			asteroidDead = false;
			asteroidDestroyed.setAnimationComplete(true);
			asteroidDestroyed.setCurrentFrame(AsteroidNS::ENEMY_DESTROYED_START);
		}
	}

}

void Asteroid::sendAsteroid(){
	int randX = rand()%500 + (-150);
	velocity.x = randX;
	velocity.y = 250;

	//spriteData.x = (rand()%500) + 20; //sets the bullets x coordinate to whereever the EnemyShip was when it fired
	//spriteData.y = (-40); //sets the bullets y coordinate to whereever the EnemyShip was when it fired
}

//TODO: FINISH RESET and put it in game
void Asteroid::reset(){
	asteroidDead = false;
	timeInState = 0.0f;
	sent = false;
	this->setVisible(true); 
	this->setActive(true);
	this->setX(rand()%550);                    // start above and left of planet
	this->setY(-70);
	velocity.x = 0;
	velocity.y = 0;
	asteroidDestroyed.setCurrentFrame(AsteroidNS::ENEMY_DESTROYED_START);
}
