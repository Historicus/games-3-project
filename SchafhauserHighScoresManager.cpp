#include "SchafhauserHighScoresManager.h"
HighScoresManager::HighScoresManager(std::string playerName, int score){

	//if the player didn't enter a name
	if ((playerName == "") || (playerName == "\r")){
		playerName = "N\/A";
	}

	//this is where we will figure out how many periods to add
		//nick     to    nick..........
	/*int extraPeriods = 10 - playerName.length();
	for(int i = 0; i < 10 + extraPeriods; i++){
		playerName += ' .';
	}*/

	std::ifstream fin;
	fin.open("highscores.txt");
	
	std::string storedPlayerName;
	int storedScore;

	while(fin >> storedPlayerName >> storedScore){
		names.push_back(storedPlayerName);
		scores.push_back(storedScore);
	}

	fin.close();

	//find where to place the new score and playerName.
	int index = scores.size();
	for(int i = 0; i < scores.size(); i++){
		if(score >= scores[i]){
			index = i;
			break;
		}
	}

	scores.insert(scores.begin()+index, score);
	names.insert(names.begin()+index, playerName);

	//if we have more than 10 high scores, delete them until we have 10
	if (scores.size() > 10){
		scores.pop_back();
		names.pop_back();
	}

	//write the scores back to the file.
	std::ofstream fout;
	fout.open("highscores.txt");

	for (int i = 0; i < names.size(); i++){
		fout << names[i] << " " << scores[i] << std::endl;
	}

	fout.close();
}
