// Programming 2D Games
// Copyright (c) 2011,2012 by: 
// Charles Kelly
// collisionTypes.h v1.0

#ifndef _COLLISION_TYPES_H      // Prevent multiple definitions if this 
#define _COLLISION_TYPES_H      // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

class CollisionTypes;

#include "game.h"
#include "textDX.h"
#include "menu.h"
#include "DaviesEnemyShip.h"
#include "KenneyAIShip.h"
#include <sstream>
#include <string>
#include "KenneyPlayerShip.h"
#include "SchafhauserAsteroid.h"
#include "Schafhauserheart.h"
#include "DaviesBullet.h"
#include "SchafhauserAsteroidManager.h"
#include "SchafhauserHighScoresManager.h"
#include "patternStepAIShip.h"


#define maxPatternSteps 3
//=============================================================================
// This class is the core of the game
//=============================================================================
class CollisionTypes : public Game
{
private:
	TextureManager splashTexture1;
	Image splashScreen1;

	TextureManager splashTexture2;
	Image splashScreen2;
	
	TextureManager menuTexture;
	Image menuScreen;

	TextureManager backTexture;
	Image backScreen;

	TextureManager titleTexture;
	Image titleScreen;

	TextureManager highscoresTexture;
	Image highscores;

	TextureManager threeTexture;
	TextureManager twoTexture;
	TextureManager oneTexture;
	TextureManager goTexture;
	Image three;
	Image two;
	Image one;
	Image go;
	bool setmove;
	bool level2occured;
	TextureManager level1Texture;
	Image level1;
	TextureManager level2Texture;
	Image level2;
	TextureManager gameoverTexture;
	Image gameoverScreen;
	TextureManager youwinTexture;
	Image youwinScreen;

	// ADD FONT here
	TextDX *scoreMessage;
	TextDX *finalScoreMessage;
	TextDX *playerName;
	std::string charStringForPlayerName;
	TextDX *highscoresMessage[10];
	TextDX *enterNameInstructionsMessage;
	TextDX *nameMessage;
	//std::stringstream highscoresText[10];
	//int score;

	Menu *mainMenu;
	EnemyShip Enemy[8];
	AIShip intel;
	PlayerShip Player; 
	Bullet PlayerBullets[100]; //100 bullets for playership
	EnemyBullet Enemy1Bullets[9][40];
	TextDX *output;
	std::string outString;

	Asteroid asteroids[50];
	//AsteroidManager MyAsteroidManager;

	TextDX *outputBack;
	std::string backString;
	int numOfAsteroids;
	// STATE Stuff
	/*GameStates gameStates;*/
	//Image splashScreen;
	//TextureManager splashScreenTM;
	//Image gameOver;
	//TextureManager gameOverTM;
	float timeInState;
	void gameStateUpdate();
	int count;
	/*TextureManager GameTextures;
	PlayerShip Player;*/

	bool isPlayerDoneEnteringName;

	HighScoresManager *myHighScoresManager;

	PatternStep patternSteps[maxPatternSteps];
	int patternStepIndex;
	bool AI;

	bool beforeTrack;
public:
	GameStates gameStates;
	shake shaker;
	TextureManager GameTextures;
	bool level; //true for level 1, false for level 2
	heart Hearts;
	int score;
	int numAsteroids;
    // Constructor
    CollisionTypes();
	VECTOR2 foo, bar;
    // Destructor
    virtual ~CollisionTypes();
	bool first;
	bool second;
    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
};

#endif
