
#include "submenu.h"

subMenu::subMenu()
{
	selectedItem = -1;	//nothing return
	menuItemFont = new TextDX();
	menuHeadingFont = new TextDX();
}

void subMenu::initialize(Graphics *g, Input *i)
{
	submenu1 = "First";
	submenu2 = "Second";
	submenu3 = "Third";

	highlightColor = graphicsNS::RED;
	normalColor = graphicsNS::WHITE;
	menuAnchor = D3DXVECTOR2(270,10);
	input = i;
	verticalOffset = 30;
	linePtr = 0;
	selectedItem = -1;
	graphics = g;
	menuItemFont = new TextDX();
	menuHeadingFont = new TextDX();
	menuItemFontHighlight = new TextDX();
	if(menuItemFont->initialize(graphics, 15, true, false, "Calibri") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if(menuItemFontHighlight->initialize(graphics, 18, true, false, "Calibri") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if(menuHeadingFont->initialize(graphics, 25, true, false, "Calibri") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuHeading font"));
	menuHeadingFont->setFontColor(normalColor);
	menuItemFont->setFontColor(normalColor);
	menuItemFontHighlight->setFontColor(highlightColor);
	upDepressedLastFrame = false;
	downDepressedLastFrame = false;
}

void subMenu::update()
{
	if (input->wasKeyPressed(VK_UP))
	{
		linePtr--;
	}
	if (input->wasKeyPressed(VK_DOWN))
	{
		linePtr++;
	}
	if (linePtr > 2) linePtr = 0;
	if (linePtr < 0) linePtr = 2;

	if (input->isKeyDown(VK_RETURN))
		selectedItem = linePtr;
	else selectedItem = -1;
}

void subMenu::displayMenu()
{
	if (linePtr==0)
		menuItemFontHighlight->print(submenu1, menuAnchor.x, menuAnchor.y+verticalOffset);
	else
		menuItemFont->print(submenu1, menuAnchor.x, menuAnchor.y+verticalOffset);
	int foo = 2*verticalOffset;
	if (linePtr==1)
		menuItemFontHighlight->print(submenu2, menuAnchor.x, menuAnchor.y+foo);
	else
		menuItemFont->print(submenu2, menuAnchor.x, menuAnchor.y+foo);
	foo = 3*verticalOffset;
	if (linePtr==2)
		menuItemFontHighlight->print(submenu3, menuAnchor.x, menuAnchor.y+foo);
	else
		menuItemFont->print(submenu3, menuAnchor.x, menuAnchor.y+foo);
}