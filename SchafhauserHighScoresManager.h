#pragma once
#include <string>
#include <fstream>
#include <vector>
class HighScoresManager
{
private:
public:
	HighScoresManager(std::string playerName, int score);

	std::vector<std::string> names;
	std::vector<int> scores;
};