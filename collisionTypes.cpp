

#include "collisionTypes.h"
#include "DaviesEnemyShip.h"
#include "SmokeparticleManager.h"
#include "PlayerBulletParticleManager.h"
#include "AIBulletparticleManager.h"

SmokeParticleManager pmAI;
SmokeParticleManager pmShip;
PlayerBulletParticleManager pmBullet;
AIBulletParticleManager pmEnemy;

//=============================================================================
// Constructor
//=============================================================================
CollisionTypes::CollisionTypes()
{
	//nothing here, move on
}

//=============================================================================
// Destructor
//=============================================================================
CollisionTypes::~CollisionTypes()
{
	releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void CollisionTypes::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // throws GameError

	setmove = true;

#pragma region initilizeImages
	//splash screen
	if (!splashTexture1.initialize(graphics,BACK_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing splash screen texture"));
	// splash image
	if (!splashScreen1.initialize(graphics,0,0,0,&splashTexture1))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing splash screen"));
	//splash screen
	if (!splashTexture2.initialize(graphics,BACK_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing splash screen texture"));
	// splash image
	if (!splashScreen2.initialize(graphics,0,0,0,&splashTexture2))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing splash screen"));
	splashScreen2.setY(-splashScreen2.getHeight());

	//title screen
	if (!titleTexture.initialize(graphics,TITLE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing title screen texture"));
	// title image
	if (!titleScreen.initialize(graphics,0,0,0,&titleTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing title screen"));
	titleScreen.setX(3);
	titleScreen.setY(50);

	//level1 screen
	if (!level1Texture.initialize(graphics,LEVEL1_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing go screen texture"));
	// level1 image
	if (!level1.initialize(graphics,0,0,0,&level1Texture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing go screen"));
	level1.setX(65);
	level1.setY(232);
	level1.setScale(.8f);

	//level2 screen
	if (!level2Texture.initialize(graphics,LEVEL2_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing go screen texture"));
	// level2 image
	if (!level2.initialize(graphics,0,0,0,&level2Texture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing go screen"));
	level2.setX(65);
	level2.setY(232);
	level2.setScale(.8f);

	//gameover screen
	if (!gameoverTexture.initialize(graphics,GAMEOVER_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing gameover screen texture"));
	// gameover image
	if (!gameoverScreen.initialize(graphics,0,0,0,&gameoverTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing gameover screen"));
	gameoverScreen.setX(65);
	gameoverScreen.setY(232);
	gameoverScreen.setScale(0.6f);
	//youwin screen
	if (!youwinTexture.initialize(graphics,YOUWIN_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing youwin screen texture"));
	// youwin image
	if (!youwinScreen.initialize(graphics,0,0,0,&youwinTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing youwin screen"));
	youwinScreen.setX(85);
	youwinScreen.setY(232);
	youwinScreen.setScale(.6f);

	//three screen
	if (!threeTexture.initialize(graphics,THREE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing three screen texture"));
	// three image
	if (!three.initialize(graphics,0,0,0,&threeTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing three screen"));
	three.setX(235);
	three.setY(232);

	//two screen
	if (!twoTexture.initialize(graphics,TWO_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing two screen texture"));
	// two image
	if (!two.initialize(graphics,0,0,0,&twoTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing two screen"));	
	two.setX(235);
	two.setY(232);

	//one screen
	if (!oneTexture.initialize(graphics,ONE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing one screen texture"));
	// one image
	if (!one.initialize(graphics,0,0,0,&oneTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing one screen"));	
	one.setX(235);
	one.setY(232);

	//go screen
	if (!goTexture.initialize(graphics,GO_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing go screen texture"));
	// go image
	if (!go.initialize(graphics,0,0,0,&goTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing go screen"));
	go.setX(180);
	go.setY(232);

	//highscores
	if (!highscoresTexture.initialize(graphics,HIGHSCORES_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing highscores texture"));
	// go image
	if (!highscores.initialize(graphics,0,0,0,&highscoresTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing highscores screen"));
	highscores.setScale(0.7f);
	highscores.setX(50);
	highscores.setY(50);


	level2occured = false;

#pragma endregion
#pragma region InitializeMenu
	mainMenu = new Menu();
	mainMenu->initialize(graphics, input, this);
	outString = "Selected Item: ";
	output = new TextDX();
	if(output->initialize(graphics, 15, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing output font"));
#pragma endregion
#pragma region PlayerShipInitialization
	if (!GameTextures.initialize(graphics, TEXTURES_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing baron game textures"));

	if (!Player.initialize(this, PlayerNS::WIDTH, PlayerNS::HEIGHT, PlayerNS::TEXTURE_COLS, &GameTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing EnemyShip"));
	Player.setX(GAME_WIDTH/2-50);                    // start above and left of planet
	Player.setY(GAME_HEIGHT - (PlayerNS::HEIGHT + 20));
	Player.setFrames(PlayerNS::PLAYER_IDLE_START, PlayerNS::PLAYER_IDLE_END);   // animation frames
	Player.setCurrentFrame(PlayerNS::PLAYER_IDLE_START);     // starting frame
	Player.setFrameDelay(PlayerNS::PLAYER_ANIMATION_DELAY);
	Player.setScale(PlayerNS::PLAYER_IMAGE_SCALE);
	Player.setVelocity(VECTOR2(100,0));

#pragma endregion
#pragma region EnemyShipInitialization
	for(int i =0; i < 4; i++)
	{
		if (!Enemy[i].initialize(this, EnemyNS::WIDTH, EnemyNS::HEIGHT, EnemyNS::TEXTURE_COLS, &GameTextures))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing EnemyShip"));
		Enemy[i].setX(i*70);
		Enemy[i].setY(-100);
		Enemy[i].setFrames(EnemyNS::ENEMY_IDLE_START, EnemyNS::ENEMY_IDLE_END);   // animation frames
		Enemy[i].setCurrentFrame(EnemyNS::ENEMY_IDLE_START);     // starting frame
		Enemy[i].setFrameDelay(EnemyNS::ENEMY_ANIMATION_DELAY);
		Enemy[i].setScale(EnemyNS::ENEMY_IMAGE_SCALE);
		//Enemy[i].setVelocity(VECTOR2(100,0));
		//Enemy.setDead(false);
	}
	int count = 0;
	for(int j = 4; j < 8; j++)
	{
		count++;
		if (!Enemy[j].initialize(this, EnemyNS::WIDTH, EnemyNS::HEIGHT, EnemyNS::TEXTURE_COLS, &GameTextures))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing EnemyShip"));
		Enemy[j].setX(GAME_WIDTH - count*70);
		Enemy[j].setY(-100);
		Enemy[j].setFrames(EnemyNS::ENEMY_IDLE_START, EnemyNS::ENEMY_IDLE_END);   // animation frames
		Enemy[j].setCurrentFrame(EnemyNS::ENEMY_IDLE_START);     // starting frame
		Enemy[j].setFrameDelay(EnemyNS::ENEMY_ANIMATION_DELAY);
		Enemy[j].setScale(EnemyNS::ENEMY_IMAGE_SCALE);
		//Enemy[j].setVelocity(VECTOR2(-100,0));
		//count++;
	}


	if (!intel.initialize(this, AINS::WIDTH, AINS::HEIGHT, AINS::TEXTURE_COLS, &GameTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing EnemyShip"));
	intel.setX(GAME_WIDTH/2);                    // start above and left of planet
	intel.setY(0);
	intel.setFrames(AINS::AI_IDLE_START, AINS::AI_IDLE_END);   // animation frames
	intel.setCurrentFrame(AINS::AI_IDLE_START);     // starting frame
	intel.setFrameDelay(AINS::AI_ANIMATION_DELAY);
	intel.setScale(AINS::AI_IMAGE_SCALE);
	intel.setVelocity(VECTOR2(0,0));
#pragma endregion 
#pragma region HeartsInitialization
	if (!GameTextures.initialize(graphics, TEXTURES_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing baron game textures"));

	if (!Hearts.initialize(this, heartNS::WIDTH, heartNS::HEIGHT, heartNS::TEXTURE_COLS, &GameTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing EnemyShip"));
	Hearts.setX(GAME_WIDTH/2-50);                    // start above and left of planet
	Hearts.setY(GAME_HEIGHT - (heartNS::HEIGHT + 5));
	Hearts.setFrames(heartNS::HEART3_START, heartNS::HEART3_END);   // animation frames
	Hearts.setCurrentFrame(heartNS::HEART3_START);     // starting frame
	Hearts.setFrameDelay(heartNS::HEART_ANIMATION_DELAY);
	Hearts.setScale(heartNS::HEART_IMAGE_SCALE);
	Hearts.setVelocity(VECTOR2(100,0));

#pragma endregion
#pragma region Asteroid1Initialization

	for (int i = 0; i < 50; i++){
		if (!asteroids[i].initialize(this, AsteroidNS::WIDTH, AsteroidNS::HEIGHT, AsteroidNS::TEXTURE_COLS, &GameTextures))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing EnemyShip"));
		asteroids[i].setX(rand()%550);                    // start above and left of planet
		asteroids[i].setY(-70);
		asteroids[i].setFrames(AsteroidNS::ASTEROID_IDLE_START, AsteroidNS::ASTEROID_IDLE_END);   // animation frames
		asteroids[i].setCurrentFrame(AsteroidNS::ASTEROID_IDLE_START);     // starting frame
		asteroids[i].setFrameDelay(AsteroidNS::ASTEROID_ANIMATION_DELAY);
		asteroids[i].setScale(AsteroidNS::ASTEROID_IMAGE_SCALE);
		asteroids[i].setVelocity(VECTOR2(0,0));
	}
#pragma endregion
#pragma region PlayerBulletInitialization
	//if (!GameTextures.initialize(graphics, TEXTURES_IMAGE))
	//	throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing baron game textures"));

	//initialize 100 bullets for PlayerShip
	for (int i = 0; i < 100; i++){
		if (!PlayerBullets[i].initialize(this, BulletNS::WIDTH, BulletNS::HEIGHT, BulletNS::TEXTURE_COLS, &GameTextures))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing PlayerShip Bullet " + i));
		PlayerBullets[i].setX(GAME_WIDTH/2);                    // start above and left of planet
		PlayerBullets[i].setY(GAME_HEIGHT/2 - (BulletNS::HEIGHT + 50));
		PlayerBullets[i].setFrames(BulletNS::PLAYER_BULLET_START, BulletNS::PLAYER_BULLET_END);   // animation frames
		PlayerBullets[i].setCurrentFrame(BulletNS::PLAYER_BULLET_START);     // starting frame
		PlayerBullets[i].setFrameDelay(BulletNS::BULLET_ANIMATION_DELAY);
		PlayerBullets[i].setScale(BulletNS::BULLET_IMAGE_SCALE);
		PlayerBullets[i].setVelocity(VECTOR2(0,-200));
		PlayerBullets[i].setScale(0.7f);
		PlayerBullets[i].setVisible(false);
		Player.bulletVec.push_back(PlayerBullets[i]);
	}
#pragma endregion
#pragma region Enemy1BulletInitialization
	for (int j = 0; j < 9; j++){
		for (int i = 0; i < 40; i++){
			if (!Enemy1Bullets[j][i].initialize(this, EnemyBulletNS::WIDTH, EnemyBulletNS::HEIGHT, EnemyBulletNS::TEXTURE_COLS, &GameTextures))
				throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing Enemy1Ship Bullet " + i));
			Enemy1Bullets[j][i].setX(GAME_WIDTH/2);                    // start above and left of planet
			Enemy1Bullets[j][i].setY(GAME_HEIGHT - (EnemyBulletNS::HEIGHT + 50));
			Enemy1Bullets[j][i].setFrames(EnemyBulletNS::ENEMY_BULLET_START, EnemyBulletNS::ENEMY_BULLET_END);   // animation frames
			Enemy1Bullets[j][i].setCurrentFrame(EnemyBulletNS::ENEMY_BULLET_START);     // starting frame
			Enemy1Bullets[j][i].setFrameDelay(EnemyBulletNS::BULLET_ANIMATION_DELAY);
			Enemy1Bullets[j][i].setScale(EnemyBulletNS::BULLET_IMAGE_SCALE);
			Enemy1Bullets[j][i].setVelocity(VECTOR2(0,200));
			Enemy1Bullets[j][i].flipVertical(true);
			Enemy1Bullets[j][i].setScale(0.7f);
			if (j < 8){
				Enemy[j].bulletVec.push_back(Enemy1Bullets[j][i]);
			} else {
				intel.bulletVec.push_back(Enemy1Bullets[j][i]);
			}
		}
	}
#pragma endregion
#pragma region audioStuff
	//CHANGE BACK
	audio->playCue(BACKGROUNDMUSIC);
#pragma endregion
#pragma region InitializeScoreMessage
	scoreMessage = new TextDX();
	if(scoreMessage->initialize(graphics, 30, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
	//score = 0;

	finalScoreMessage = new TextDX();
	if(finalScoreMessage->initialize(graphics, 35, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
#pragma endregion
#pragma region InitializeEnterPlayerNameMessage
	playerName = new TextDX();
	if(playerName->initialize(graphics, 40, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	enterNameInstructionsMessage = new TextDX();
	if(enterNameInstructionsMessage->initialize(graphics, 39, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	nameMessage = new TextDX();
	if(nameMessage->initialize(graphics, 39, true, false, "Courier") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
#pragma endregion
#pragma region InitalizeHighScoresMessage
	for(int i = 0; i < 10; i++){
		highscoresMessage[i] = new TextDX();
		if(highscoresMessage[i]->initialize(graphics, 37, true, false, "Courier") == false)
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
	}
#pragma endregion
	//state stuff
	gameStates = intro;
	timeInState = 0;
	level = true; //TODO:
	score = 0;
	isPlayerDoneEnteringName = false;
	numAsteroids = 0;
	count = 0;

	patternStepIndex = 0;
	for (int i = 0; i < maxPatternSteps; i++)
	{
		patternSteps[i].initialize(&intel);
		patternSteps[i].setActive();
	}
	patternSteps[0].setAction(SEND);
	patternSteps[0].setTimeForStep(0.1);
	patternSteps[1].setAction(TRACK);
	patternSteps[1].setTimeForStep(4);
	patternSteps[2].setAction(BLIND);
	patternSteps[2].setTimeForStep(2);

	beforeTrack = false;
	AI = false;
	pmAI.initialize(graphics);
	pmShip.initialize(graphics);
	pmBullet.initialize(graphics);
	pmEnemy.initialize(graphics);
	shaker = left;

	first, second = false;

	return;
}

void createParticleEffectAI(VECTOR2 pos, VECTOR2 vel, int numParticles){

	pmAI.setPosition(pos);
	pmAI.setVelocity(vel);
	pmAI.setVisibleNParticles(numParticles);
}
void createParticleEffectSHIP(VECTOR2 pos, VECTOR2 vel, int numParticles){
	pmShip.setPosition(pos);
	pmShip.setVelocity(vel);
	pmShip.setVisibleNParticles(numParticles);
}
void createParticleEffectBULLET(VECTOR2 pos, VECTOR2 vel, int numParticles){
	pmBullet.setPosition(pos);
	pmBullet.setVelocity(vel);
	pmBullet.setVisibleNParticles(numParticles);
}
void createParticleEffectENEMYBULLET(VECTOR2 pos, VECTOR2 vel, int numParticles){
	pmEnemy.setPosition(pos);
	pmEnemy.setVelocity(vel);
	pmEnemy.setVisibleNParticles(numParticles);
}

void CollisionTypes::gameStateUpdate()
{
	timeInState += frameTime;
	if (gameStates==intro && timeInState > 1.5)
	{
		score = 0;
		numAsteroids = 0;
		gameStates = readyToPlay;
		timeInState = 0;
	}
	if (gameStates==readyToPlay && mainMenu->playGame)
	{
		mainMenu->playGame = false;
		gameStates =  levelOne;
		if (mainMenu->skipLevel){ //if skip level cheat code is enabled.
			gameStates = levelTwo;
			level2occured = true;
			Player.level2 = true;
			level = false;
			Player.level = false;
		}
		timeInState = 0;
	}
	if (gameStates== levelOne && timeInState > 1.3){
		gameStates = threeState;
		timeInState = 0;
	}
	if (gameStates== threeState && timeInState > .9){
		gameStates = twoState;
		timeInState = 0;
	}
	if (gameStates== twoState && timeInState >1){
		gameStates = oneState;
		timeInState = 0;
	}	
	if (gameStates== oneState && timeInState >1){
		gameStates = goState;
		timeInState = 0;
	}
	
	if (gameStates== goState && timeInState >1){
		if (level){
			gameStates = gamePlay; //revert back to gamePlay1. just for testing gameplay2
		}
		else{
			gameStates = gamePlay2;
		}
		timeInState = 0;
	}

	if(gameStates == gamePlay && timeInState < 1)
	{
		for(int i = 0; i < 8; i++)
		{
			Enemy[i].setVelocity(VECTOR2(0,75));
		}			
	}
	if(gameStates == gamePlay && setmove && timeInState > 3)
	{
		setmove = false;
		for(int i = 0; i < 4; i++)
		{
			Enemy[i].setVelocity(VECTOR2(100,0));
		}
		for(int j = 4; j < 8; j++)
		{
			Enemy[j].setVelocity(VECTOR2(-100,0));
		}

	}

	if (gameStates == levelTwo && timeInState > 1.3){
		//reset all of enemy ship and intel ship bullets

		//TODO
		for (int j = 0; j < 8; j++){
			int numberOfEnemyBulletsOnScreen = Enemy[j].bulletsOnScreen.size();
			for(int i = 0; i < numberOfEnemyBulletsOnScreen;){
				Enemy[j].bulletsOnScreen[i].setInvisible();
				Enemy[j].recycleBullet(i);
				numberOfEnemyBulletsOnScreen--;
			}
		}
		int numberOfIntelBulletsOnScreen = intel.bulletsOnScreen.size();
		for (int i = 0; i < numberOfIntelBulletsOnScreen;){
			intel.bulletsOnScreen[i].setInvisible();
			intel.recycleBullet(i);
			numberOfIntelBulletsOnScreen--;
		}

		gameStates = threeState;
		timeInState = 0;
	}

	if(gameStates == GameOver && timeInState > 2.5 && level){
		gameStates = intro;
		timeInState = 0;
		//resets
		setmove = true;
		score = 0;
		Player.reset();
		for(int i =0; i < 4; i++)
		{
			Enemy[i].reset(i*70, 0);

			//get rid of any enemy bullets on the screen
			int numberOfEnemiesBulletsOnScreen = Enemy[i].bulletsOnScreen.size();
			for(int p = 0; p < numberOfEnemiesBulletsOnScreen;){
				Enemy[i].bulletsOnScreen[p].setInvisible();
				Enemy[i].recycleBullet(p);
				numberOfEnemiesBulletsOnScreen--;
			}
		}
		int count = 0;
		for(int j = 4; j < 8; j++)
		{
			count++;
			Enemy[j].reset(GAME_WIDTH - count*70, 0);

			//get rid of any enemy bullets on the screen
			int numberOfEnemiesBulletsOnScreen = Enemy[j].bulletsOnScreen.size();
			for(int k = 0; k < numberOfEnemiesBulletsOnScreen;){
				Enemy[j].bulletsOnScreen[k].setInvisible();
				Enemy[j].recycleBullet(k);
				numberOfEnemiesBulletsOnScreen--;
			}
			/*
			Enemy[j].setX(GAME_WIDTH - count*70);
			Enemy[j].setY(160);*/
		}

		//gets ride of player bullets on the screen
		int numberOfPlayersBulletsOnScreen = Player.bulletsOnScreen.size();
		for (int y = 0; y < numberOfPlayersBulletsOnScreen;){
			Player.bulletsOnScreen[y].setInvisible();
			Player.recycleBullet(y);
			numberOfPlayersBulletsOnScreen--;
		}
		Hearts.reset();

		intel.reset(0,0);
		//get rid of AI bullets on the screen
		int numberOfIntelBulletsOnScreen = intel.bulletsOnScreen.size();
		for (int i = 0; i < numberOfIntelBulletsOnScreen;){
			intel.bulletsOnScreen[i].setInvisible();
			intel.recycleBullet(i);
			numberOfIntelBulletsOnScreen--;
		}


	}
	if(gameStates == GameOver && timeInState > 1.3 && level == false){
		setmove = true;
		//we died in level 2
		gameStates = levelTwo;
		timeInState = 0;
		int numberOfPlayersBulletsOnScreen = Player.bulletsOnScreen.size();
		for (int y = 0; y < numberOfPlayersBulletsOnScreen;){
			Player.bulletsOnScreen[y].setInvisible();
			Player.recycleBullet(y);
			numberOfPlayersBulletsOnScreen--;
		}
		Player.reset();
		Hearts.reset();

		for(int i = 0; i < 50; i++){
			asteroids[i].reset();
		}

	}

	if(gameStates == YouWin && timeInState > 1){
		level = true;
		Player.level = true;


		//resets
		setmove = true;

		//Player.reset();
		for(int i =0; i < 4; i++)
		{
			Enemy[i].reset(i*70, 0);

			//get rid of any enemy bullets on the screen
			int numberOfEnemiesBulletsOnScreen = Enemy[i].bulletsOnScreen.size();
			for(int p = 0; p < numberOfEnemiesBulletsOnScreen;){
				Enemy[i].bulletsOnScreen[p].setInvisible();
				Enemy[i].recycleBullet(p);
				numberOfEnemiesBulletsOnScreen--;
			}
		}
		int count = 0;
		for(int j = 4; j < 8; j++)
		{
			count++;
			Enemy[j].reset(GAME_WIDTH - count*70, 0);

			//get rid of any enemy bullets on the screen
			int numberOfEnemiesBulletsOnScreen = Enemy[j].bulletsOnScreen.size();
			for(int k = 0; k < numberOfEnemiesBulletsOnScreen;){
				Enemy[j].bulletsOnScreen[k].setInvisible();
				Enemy[j].recycleBullet(k);
				numberOfEnemiesBulletsOnScreen--;
			}

		}

		int numberOfPlayersBulletsOnScreen = Player.bulletsOnScreen.size();
		for (int y = 0; y < numberOfPlayersBulletsOnScreen;){
			Player.bulletsOnScreen[y].setInvisible();
			Player.recycleBullet(y);
			numberOfPlayersBulletsOnScreen--;
		}
		Hearts.reset();


		intel.reset(0,0);
		//get rid of AI bullets on the screen
		int numberOfIntelBulletsOnScreen = intel.bulletsOnScreen.size();
		for (int i = 0; i < numberOfIntelBulletsOnScreen;){
			intel.bulletsOnScreen[i].setInvisible();
			intel.recycleBullet(i);
			numberOfIntelBulletsOnScreen--;
		}


		for(int i = 0; i < 50; i++){
			asteroids[i].reset();
		}

		//when the player has entered their name and then hit the enter key
		if (isPlayerDoneEnteringName){
			gameStates = highScores;
			timeInState = 0;

			//HighScoresManager myHighScoresManager(charStringForPlayerName, score);
			myHighScoresManager = new HighScoresManager(charStringForPlayerName, score);
			charStringForPlayerName.clear();

			Player.reset();
			isPlayerDoneEnteringName = false;
		}
	}
	if(gameStates == highScores && timeInState > 5){
		//if(input->wasKeyPressed(ENTER_KEY)){
		timeInState = 0;
		gameStates = intro;
		//}
	}
}

//=============================================================================
// Update all game items
//=============================================================================
void CollisionTypes::update()
{
	//VECTOR2 foo, bar;
	//mainMenu->update();
	float y1 = splashScreen1.getY() + 60 * frameTime;
	float y2 = splashScreen2.getY() + 60 * frameTime;

	gameStateUpdate();
	switch (gameStates)
	{
	case intro:
		//display splash screen
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);
		level2occured = false;

		pmShip.update(frameTime);
		pmEnemy.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
		break;
	case readyToPlay:
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		Player.setShooting(false);
		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );

		//menuScreen.update(frameTime);
		mainMenu->update();
		break;
	case levelOne:
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		Player.setShooting(false);
		Player.update(frameTime);
		Hearts.update(frameTime);

		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
		level1.update(frameTime);
		break;
	case levelTwo:
		
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);
		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
		level2.update(frameTime);
		splashScreen1.setY(y1);
		splashScreen2.setY(y2);
		
		Player.setShooting(false);
		Player.update(frameTime);
		Hearts.update(frameTime);

		pmAI.update(frameTime);
		pmShip.update(frameTime);
		pmEnemy.update(frameTime);
		pmBullet.update(frameTime);

		for (int j = 0; j < intel.bulletsOnScreen.size(); j++){
			foo = VECTOR2(intel.bulletsOnScreen[j].getCenterX()-6, intel.bulletsOnScreen[j].getCenterY());
			bar = VECTOR2(-10,30);
			createParticleEffectENEMYBULLET(foo, bar, 50);
		}

		for (int i = 0; i < 8; i++){
			for (int k = 0; k < Enemy[i].bulletsOnScreen.size(); k++){
				foo = VECTOR2(Enemy[i].bulletsOnScreen[k].getCenterX()-6, Enemy[i].bulletsOnScreen[k].getCenterY());
				bar = VECTOR2(-10,30);
				createParticleEffectENEMYBULLET(foo, bar, 50);
			}
		}

		if ((!(mainMenu->skipLevel))&& (!level2occured)){
			
			for(int i = 0; i < 8; i++)
			{
				Enemy[i].update(frameTime);
			}
			intel.update(frameTime);
		}
		break;
	case threeState:
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		if (!level2occured){
			pmAI.update(frameTime);
			pmShip.update(frameTime);
		}
		pmBullet.update(frameTime);
		Player.update(frameTime);
		Hearts.update(frameTime);
		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
		three.update(frameTime);
		break;
	case twoState:
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		pmAI.update(frameTime);
		pmShip.update(frameTime);
		pmBullet.update(frameTime);

		Player.update(frameTime);
		Hearts.update(frameTime);
		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
		two.update(frameTime);
		break;
	case oneState:
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		pmAI.update(frameTime);
		pmShip.update(frameTime);
		pmBullet.update(frameTime);

		Player.update(frameTime);
		Hearts.update(frameTime);
		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
		one.update(frameTime);
		break;
	case goState:
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		pmAI.update(frameTime);
		pmShip.update(frameTime);
		pmBullet.update(frameTime);

		Player.update(frameTime);
		Hearts.update(frameTime);
		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
		go.update(frameTime);
		break;
	case gamePlay:
		

		AI = true;
		Player.setShooting(true);
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );

		Player.update(frameTime);

		//if (!Player.getHit())
			Hearts.update(frameTime);

		for(int i = 0; i < 8; i++)
		{
			Enemy[i].update(frameTime);
		}

		for(int i = 0; i < Player.bulletsOnScreen.size(); i++){
			foo = VECTOR2(Player.bulletsOnScreen[i].getCenterX()-6, Player.bulletsOnScreen[i].getCenterY());
			bar = VECTOR2(-10,-30);
			createParticleEffectBULLET(foo, bar, 50);
		}

		for (int j = 0; j < intel.bulletsOnScreen.size(); j++){
			foo = VECTOR2(intel.bulletsOnScreen[j].getCenterX()-6, intel.bulletsOnScreen[j].getCenterY());
			bar = VECTOR2(-10,30);
			createParticleEffectENEMYBULLET(foo, bar, 50);
		}

		for (int i = 0; i < 8; i++){
			for (int k = 0; k < Enemy[i].bulletsOnScreen.size(); k++){
				foo = VECTOR2(Enemy[i].bulletsOnScreen[k].getCenterX()-6, Enemy[i].bulletsOnScreen[k].getCenterY());
				bar = VECTOR2(-10,30);
				createParticleEffectENEMYBULLET(foo, bar, 50);
			}
		}



		intel.update(frameTime);

		pmAI.update(frameTime);
		pmShip.update(frameTime);
		pmBullet.update(frameTime);
		pmEnemy.update(frameTime);

		break;
	case gamePlay2: //asteroid level
		level2occured = true;
		Player.level2 = true;
		Player.setShooting(true);
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		for (int i = 0; i < 50; i++){
			asteroids[i].update(frameTime);
		}
		timeInState += frameTime;
		for (int j = 0; j < 50; j++){
			if ((timeInState > 0.8) && (!asteroids[j].getSent())) {
				timeInState = 0;
				asteroids[j].setSent(true);
				asteroids[j].sendAsteroid();
			}

		}



		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );

		for(int i = 0; i < Player.bulletsOnScreen.size(); i++){
			foo = VECTOR2(Player.bulletsOnScreen[i].getCenterX()-6, Player.bulletsOnScreen[i].getCenterY());
			bar = VECTOR2(-10,-30);
			createParticleEffectBULLET(foo, bar, 50);
		}

		Player.update(frameTime);

		//if (!Player.getHit())
			Hearts.update(frameTime);

		count = 0;
		for (int i = 0; i < 50; i++){
			if(asteroids[i].getDead())
				count++;

			if(count >= 50){ //end of level two. go to gameOver screen.
				gameStates = YouWin;
				timeInState = 0;
				audio->playCue(APPLAUSE);
			}
		}

		pmAI.update(frameTime);
		pmShip.update(frameTime);
		pmBullet.update(frameTime);

		break;
	case GameOver:
		AI = false;
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);
		if (!mainMenu->skipLevel){
			for(int i = 0; i < 8; i++)
			{
				Enemy[i].update(frameTime);
			}	
			intel.update(frameTime);
		}

		

		if (level2occured) {
			for (int i = 0; i < 50; i++){
				asteroids[i].update(frameTime);
			}
		}
		
		for (int j = 0; j < intel.bulletsOnScreen.size(); j++){
			foo = VECTOR2(intel.bulletsOnScreen[j].getCenterX()-6, intel.bulletsOnScreen[j].getCenterY());
			bar = VECTOR2(-10,30);
			createParticleEffectENEMYBULLET(foo, bar, 50);
		}

		for (int i = 0; i < 8; i++){
			for (int k = 0; k < Enemy[i].bulletsOnScreen.size(); k++){
				foo = VECTOR2(Enemy[i].bulletsOnScreen[k].getCenterX()-6, Enemy[i].bulletsOnScreen[k].getCenterY());
				bar = VECTOR2(-10,30);
				createParticleEffectENEMYBULLET(foo, bar, 50);
			}
		}
		for(int i = 0; i < Player.bulletsOnScreen.size(); i++){
			foo = VECTOR2(Player.bulletsOnScreen[i].getCenterX()-6, Player.bulletsOnScreen[i].getCenterY());
			bar = VECTOR2(-10,-30);
			createParticleEffectBULLET(foo, bar, 50);
		}

		pmEnemy.update(frameTime);
		pmShip.update(frameTime);
		pmBullet.update(frameTime);
		Player.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
		gameoverScreen.update(frameTime);
		break;
	case YouWin:
		AI = false;
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		titleScreen.update(frameTime);

		splashScreen1.setY(y1);
		splashScreen2.setY(y2);

		Player.update(frameTime);

		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
		youwinScreen.update(frameTime);

		if (input->wasKeyPressed(ENTER_KEY)){
			isPlayerDoneEnteringName = true;
		}

		break;
	case highScores:
		splashScreen1.update(frameTime);
		splashScreen2.update(frameTime);
		splashScreen1.setY(y1);
		splashScreen2.setY(y2);
		if ((splashScreen1.getCenterY()-350) > GAME_HEIGHT)
			splashScreen1.setY(-splashScreen1.getHeight() );
		if (splashScreen2.getCenterY() - 350 > GAME_HEIGHT)
			splashScreen2.setY(-splashScreen2.getHeight() );
	
		highscores.update(frameTime);
		break;
	}

}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void CollisionTypes::ai()
{
	if (AI){
		intel.ai(frameTime, Player);
		
		if ((!intel.AIshipDead)&& (!level2occured) && (intel.getVisible())){
			timeInState += frameTime;
			if ((timeInState > 0.3) && (!beforeTrack)){
				//patternSteps[0].update(frameTime);
				//timeInState = 0;
				beforeTrack = true;
				first = true;
			} else {
				//patternSteps[1].update(frameTime);

				if (first){
					patternSteps[1].update(frameTime);
					if (patternSteps[1].isFinished()){
						patternStepIndex = 2;
						second = true;
						first = false;
						patternSteps[1].setActive();
					}
				} else if (second){
					patternSteps[2].update(frameTime);
					if (patternSteps[2].isFinished()){
						patternStepIndex = 1;
						second = false;
						first = true;
						patternSteps[2].setActive();
					}
				}
			}
		}
	}
}

//=============================================================================
// Handle collisions
//=============================================================================
void CollisionTypes::collisions()
{
	VECTOR2 collisionVector;

	switch (gameStates)
	{
	case gamePlay:
		for (int j = 0; j < 8; j++){
			for(int i = 0; i < Player.bulletsOnScreen.size(); i++){
				if (Player.bulletsOnScreen[i].collidesWith(Enemy[j], collisionVector)){
					Enemy[j].setInvisible();
					Enemy[j].setDead(true);
					Player.bulletsOnScreen[i].setInvisible();
					Player.recycleBullet(i);

					audio->playCue(ENEMYEXPLOSION);
					//increase player score by 100.
					Player.score = Player.score + 100;
					Player.numberOfEnemiesKilled++;

					if(Player.numberOfEnemiesKilled >= 9){
						gameStates = levelTwo;
						level = false;
						timeInState = 0;
						Player.level = false;
						audio->playCue(APPLAUSE);

						//gets ride of player bullets on the screen
						int numberOfPlayersBulletsOnScreen = Player.bulletsOnScreen.size();
						for (int y = 0; y < numberOfPlayersBulletsOnScreen;){
							Player.bulletsOnScreen[y].setInvisible();
							Player.recycleBullet(y);
							numberOfPlayersBulletsOnScreen--;
						}
					}
				}
			}
		}
		
		for (int j = 0; j < 8; j++){
			for(int i = 0; i < Enemy[j].bulletsOnScreen.size(); i++){
				if (Enemy[j].bulletsOnScreen[i].collidesWith(Player, collisionVector)){
					//Player.setInvisible();
					Enemy[j].bulletsOnScreen[i].setInvisible();

					/*Enemy.bulletVec.push_back(Enemy.bulletsOnScreen[i]);
					Enemy.bulletsOnScreen.erase(Enemy.bulletsOnScreen.begin() + i); */
					Enemy[j].recycleBullet(i);
					//Enemy.bulletVec.back().setVisible(true);

					audio->playCue(PLAYEREXPLOSION);

					if (!mainMenu->infiniteLives){ //if infinite lives is not on, then player loses lives when hit.
						Player.lives--; //decrement amount of lives by 1
						Player.setHit(true);
					}

					if (Player.lives <= 0){ //out of lives. Game over
						Player.setInvisible();
						Player.setDead(true);
						//GameOver
						gameStates = GameOver;
						timeInState = 0;
					}
				}
			}
		}
		for(int i = 0; i < Player.bulletsOnScreen.size(); i++){
			if (Player.bulletsOnScreen[i].collidesWith(intel, collisionVector)){
				Player.bulletsOnScreen[i].setInvisible();
				Player.recycleBullet(i);

				audio->playCue(ENEMYEXPLOSION);
				//increase player score by 100.
				//Player.score = Player.score + 100;
				//Player.numberOfEnemiesKilled++;

				intel.health--; //decrement amount of lives by 1
				intel.setHit(true);

				if (intel.health <= 0){ //out of lives. Game over
					intel.setInvisible();
					//intel.setDead(true);
					intel.AIshipDead = true;
					//GameOver
					//gameStates = GameOver;
					//timeInState = 0;
					Player.score = Player.score + 100;
					Player.numberOfEnemiesKilled++;
				}

				if(Player.numberOfEnemiesKilled >= 9){
					gameStates = levelTwo;
					level = false;
					timeInState = 0;
					Player.level = false;
					audio->playCue(APPLAUSE);
					//gets ride of player bullets on the screen
					int numberOfPlayersBulletsOnScreen = Player.bulletsOnScreen.size();
					for (int y = 0; y < numberOfPlayersBulletsOnScreen;){
						Player.bulletsOnScreen[y].setInvisible();
						Player.recycleBullet(y);
						numberOfPlayersBulletsOnScreen--;
					}

				}
			}
		}
		for(int i = 0; i < intel.bulletsOnScreen.size(); i++){
			if(intel.bulletsOnScreen[i].collidesWith(Player, collisionVector)){
				intel.bulletsOnScreen[i].setInvisible();

				/*Enemy.bulletVec.push_back(Enemy.bulletsOnScreen[i]);
				Enemy.bulletsOnScreen.erase(Enemy.bulletsOnScreen.begin() + i); */
				intel.recycleBullet(i);
				//Enemy.bulletVec.back().setVisible(true);

				audio->playCue(PLAYEREXPLOSION);

				if (!mainMenu->infiniteLives){ //if infinite lives is not on, then player loses lives when hit.
					Player.lives--; //decrement amount of lives by 1
					Player.setHit(true);
				}
				if (Player.lives <= 0){ //out of lives. Game over
					Player.setInvisible();
					Player.setDead(true);
					//GameOver
					gameStates = GameOver;
					timeInState = 0;
				}
			}
		}
		if ((intel.health < 3) && (intel.health > 0)){
			foo = VECTOR2(intel.getCenterX(), intel.getCenterY()+5);
			bar = VECTOR2(-25,30);
			createParticleEffectAI(foo, bar, 50);
		}
		if ((Player.lives < 3) && (Player.lives > 0)){
			foo = VECTOR2(Player.getCenterX(), Player.getCenterY()-5);
			bar = VECTOR2(-25,40);
			createParticleEffectSHIP(foo, bar, 50);
		}
		break;
	case gamePlay2:
		////checking if any asteroids collided with the player
		for(int j = 0; j < 50; j++){
			if (asteroids[j].collidesWith(Player, collisionVector)){
				//Player.setInvisible();
				asteroids[j].setDead(true);
				asteroids[j].setInvisible();

				Player.score = Player.score + 100;
				audio->playCue(PLAYEREXPLOSION);

				if (!mainMenu->infiniteLives){ //if infinite lives is not on, then player loses lives when hit.
					Player.lives--; //decrement amount of lives by 1
					Player.setHit(true);
				}
				if (Player.lives <= 0){ //out of lives. Game over
					Player.setInvisible();
					Player.setDead(true);
					//GameOver
					gameStates = GameOver;
					timeInState = 0;

					//gets ride of player bullets on the screen
					int numberOfPlayersBulletsOnScreen = Player.bulletsOnScreen.size();
					for (int y = 0; y < numberOfPlayersBulletsOnScreen;){
						Player.bulletsOnScreen[y].setInvisible();
						Player.recycleBullet(y);
						numberOfPlayersBulletsOnScreen--;
					}
				}
			}
		}

		//int numberOfAsteroidsOnScreen = MyAsteroidManager.AsteroidsOnScreen.size();
		//checking if player bullets collide with any asteroid on the screen
		for(int j = 0; j < 50; j++){
			for(int i = 0; i < Player.bulletsOnScreen.size(); i++){
				if ((Player.bulletsOnScreen[i].collidesWith(asteroids[j], collisionVector))&& (Player.bulletsOnScreen[i].getY() > 0)){

					asteroids[j].setDead(true);
					asteroids[j].setInvisible();

					Player.bulletsOnScreen[i].setInvisible();
					Player.recycleBullet(i);

					audio->playCue(ENEMYEXPLOSION);
					//increase player score by 100.
					Player.score = Player.score + 100;

					
					//MyAsteroidManager.numberOfAsteroidsUsed++;
				}
			}
		}
		for (int k = 0; k < 50; k++){
			if (asteroids[k].getY() > GAME_HEIGHT+150){
				asteroids[k].setDead(true);
			}
		}
		if ((Player.lives < 3) && (Player.lives > 0)){
			foo = VECTOR2(Player.getCenterX(), Player.getCenterY()-5);
			bar = VECTOR2(-25,40);
			createParticleEffectSHIP(foo, bar, 50);
		}
		break;
	}

}

//=============================================================================
// Render game items
//=============================================================================
void CollisionTypes::render()
{
	graphics->spriteBegin();                // begin drawing sprites

	std::stringstream scoreMessageText;
	std::stringstream finalScoreMessageText;
	std::stringstream playerNameText;
	std::stringstream highscoresNameText[10];
	std::stringstream highscoresScoreText[10];
	std::stringstream enterNameInstructionsText;
	std::stringstream nameText;

	//clock
	//int timer = 0;

	switch (gameStates)
	{
	case intro:
		splashScreen1.draw();
		splashScreen2.draw();
		titleScreen.draw();
		level2occured = false;
		Player.level2 = false;
		break;
	case readyToPlay:

		splashScreen1.draw();
		splashScreen2.draw();
		if (!mainMenu->onControls()){
			titleScreen.draw();
		}
		mainMenu->displayMenu();
		//output->print(ss.str(), 0,0);
		break;
	case levelOne:
		splashScreen1.draw();
		splashScreen2.draw();

		level1.draw();
		Player.draw();
		Hearts.draw();
		if(Player.lives == 3){
			Hearts.setFrames(heartNS::HEART3_START, heartNS::HEART3_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART3_START); 
		}
		else if (Player.lives == 2){
			Hearts.setFrames(heartNS::HEART2_START, heartNS::HEART2_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART2_START); 
		}
		else if (Player.lives == 1){
			Hearts.setFrames(heartNS::HEART1_START, heartNS::HEART1_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART1_START); 
		}
		else if (Player.lives == 0){
			Hearts.setFrames(heartNS::HEART0_START, heartNS::HEART0_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART0_START); 
		}
		break;
	case levelTwo:

		splashScreen1.draw();
		splashScreen2.draw();
		level2.draw();

		pmEnemy.draw();
		pmShip.draw();
		Player.draw();
		
		Hearts.draw();
		if (!(mainMenu->skipLevel)){
			for(int j =0; j < 8; j++)
			{
				Enemy[j].draw();
			}
			intel.draw();
		}

		if(Player.lives == 3){
			Hearts.setFrames(heartNS::HEART3_START, heartNS::HEART3_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART3_START); 
		}
		else if (Player.lives == 2){
			Hearts.setFrames(heartNS::HEART2_START, heartNS::HEART2_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART2_START); 
		}
		else if (Player.lives == 1){
			Hearts.setFrames(heartNS::HEART1_START, heartNS::HEART1_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART1_START); 
		}
		else if (Player.lives == 0){
			Hearts.setFrames(heartNS::HEART0_START, heartNS::HEART0_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART0_START); 
		}
		break;
	case threeState:
		splashScreen1.draw();
		splashScreen2.draw();
		three.draw();

		pmShip.draw();
		Player.draw();
		
		Hearts.draw();
		if(Player.lives == 3){
			Hearts.setFrames(heartNS::HEART3_START, heartNS::HEART3_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART3_START); 
		}
		else if (Player.lives == 2){
			Hearts.setFrames(heartNS::HEART2_START, heartNS::HEART2_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART2_START); 
		}
		else if (Player.lives == 1){
			Hearts.setFrames(heartNS::HEART1_START, heartNS::HEART1_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART1_START); 
		}
		else if (Player.lives == 0){
			Hearts.setFrames(heartNS::HEART0_START, heartNS::HEART0_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART0_START); 
		}
		break;
	case twoState:
		splashScreen1.draw();
		splashScreen2.draw();
		two.draw();

		pmShip.draw();
		Player.draw();
		
		Hearts.draw();
		if(Player.lives == 3){
			Hearts.setFrames(heartNS::HEART3_START, heartNS::HEART3_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART3_START); 
		}
		else if (Player.lives == 2){
			Hearts.setFrames(heartNS::HEART2_START, heartNS::HEART2_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART2_START); 
		}
		else if (Player.lives == 1){
			Hearts.setFrames(heartNS::HEART1_START, heartNS::HEART1_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART1_START); 
		}
		else if (Player.lives == 0){
			Hearts.setFrames(heartNS::HEART0_START, heartNS::HEART0_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART0_START); 
		}
		break;
	case oneState:
		splashScreen1.draw();
		splashScreen2.draw();
		one.draw();

		pmShip.draw();
		Player.draw();
		
		Hearts.draw();
		if(Player.lives == 3){
			Hearts.setFrames(heartNS::HEART3_START, heartNS::HEART3_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART3_START); 
		}
		else if (Player.lives == 2){
			Hearts.setFrames(heartNS::HEART2_START, heartNS::HEART2_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART2_START); 
		}
		else if (Player.lives == 1){
			Hearts.setFrames(heartNS::HEART1_START, heartNS::HEART1_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART1_START); 
		}
		else if (Player.lives == 0){
			Hearts.setFrames(heartNS::HEART0_START, heartNS::HEART0_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART0_START); 
		}
		break;
	case goState:
		splashScreen1.draw();
		splashScreen2.draw();
		go.draw();

		pmShip.draw();
		Player.draw();
		
		Hearts.draw();
		if(Player.lives == 3){
			Hearts.setFrames(heartNS::HEART3_START, heartNS::HEART3_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART3_START); 
		}
		else if (Player.lives == 2){
			Hearts.setFrames(heartNS::HEART2_START, heartNS::HEART2_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART2_START); 
		}
		else if (Player.lives == 1){
			Hearts.setFrames(heartNS::HEART1_START, heartNS::HEART1_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART1_START); 
		}
		else if (Player.lives == 0){
			Hearts.setFrames(heartNS::HEART0_START, heartNS::HEART0_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART0_START); 
		}
		break;
	case gamePlay:
		splashScreen1.draw();
		splashScreen2.draw();

		pmEnemy.draw();
		pmAI.draw();
		intel.draw();
		
		//Asteroid1.draw();
#pragma region AmountOfHearts
		if(Player.lives == 3){
			Hearts.setFrames(heartNS::HEART3_START, heartNS::HEART3_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART3_START); 
		}
		else if (Player.lives == 2){
			Hearts.setFrames(heartNS::HEART2_START, heartNS::HEART2_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART2_START); 
		}
		else if (Player.lives == 1){
			Hearts.setFrames(heartNS::HEART1_START, heartNS::HEART1_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART1_START); 
		}
		else if (Player.lives == 0){
			Hearts.setFrames(heartNS::HEART0_START, heartNS::HEART0_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART0_START); 
		}
#pragma endregion
		
		
		for(int j =0; j < 8; j++)
		{
			Enemy[j].draw();
		}

		pmBullet.draw();
		pmShip.draw();
		Player.draw();
		
		Hearts.draw();
		
#pragma region scoreDrawing
		/*std::stringstream scoreMessageText;*/
		//scoreMessageText << "Hits: ";
		//score++;
		score = Player.score;
		scoreMessageText << Player.score;
		scoreMessage->setFontColor(graphicsNS::WHITE);
		scoreMessage->print(scoreMessageText.str(), GAME_WIDTH - 80, 5);
#pragma endregion
		
		break;
	case gamePlay2: //asteroid level

		splashScreen1.draw();
		splashScreen2.draw();
#pragma region AmountOfHearts
		if(Player.lives == 3){
			Hearts.setFrames(heartNS::HEART3_START, heartNS::HEART3_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART3_START); 
		}
		else if (Player.lives == 2){
			Hearts.setFrames(heartNS::HEART2_START, heartNS::HEART2_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART2_START); 
		}
		else if (Player.lives == 1){
			Hearts.setFrames(heartNS::HEART1_START, heartNS::HEART1_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART1_START); 
		}
		else if (Player.lives == 0){
			Hearts.setFrames(heartNS::HEART0_START, heartNS::HEART0_START);   // animation frames
			Hearts.setCurrentFrame(heartNS::HEART0_START); 
		}
#pragma endregion
		pmBullet.draw();
		pmShip.draw();
		Player.draw();
		
		Hearts.draw();
		for (int i = 0; i< 50; i++){
			asteroids[i].draw();
		}

#pragma region scoreDrawing
		scoreMessageText << Player.score;
		scoreMessage->setFontColor(graphicsNS::WHITE);
		scoreMessage->print(scoreMessageText.str(), GAME_WIDTH - 80, 5);
#pragma endregion
		break;
	case GameOver:
		beforeTrack = false;
		first = false;
		second = false;
		splashScreen1.draw();
		splashScreen2.draw();
		pmEnemy.draw();
		if ((!mainMenu->skipLevel)&&(!level2occured)){
			for(int j =0; j < 8; j++)
			{
				Enemy[j].draw();
			}
			intel.draw();
		}
		if (level2occured){
			for (int i = 0; i< 50; i++){
				asteroids[i].draw();
			}
		}

		pmBullet.draw();
		Player.draw();
		gameoverScreen.draw();
		break;
	case YouWin:
		splashScreen1.draw();
		splashScreen2.draw();
		youwinScreen.draw();
#pragma region scoreDrawing
		/*std::stringstream scoreMessageText;*/
		//scoreMessageText << "Hits: ";
		//score++;
		score = Player.score * Player.lives;
		finalScoreMessageText << "Final Score: ";
		finalScoreMessageText << score;
		finalScoreMessage->setFontColor(graphicsNS::ORANGE);
		/*finalScoreMessage->print(finalScoreMessageText.str(), 170, 270);*/
		RECT box;
		box.left = 90; //20
		box.top = 300;
		box.right = 450; //450
		box.bottom = 400;
		
		finalScoreMessage->print(finalScoreMessageText.str(), box, DT_CENTER);
		
		enterNameInstructionsText << "Enter your name below \n  and press \"Enter\"";
		enterNameInstructionsMessage->setFontColor(graphicsNS::WHITE);
		enterNameInstructionsMessage->print(enterNameInstructionsText.str(), 7, 350);

		nameText << "Name:";
		nameMessage->setFontColor(graphicsNS::WHITE);
		nameMessage->print(nameText.str(), 140, 450);
		//score = 0;
#pragma endregion

#pragma region playerNameEntry
		if(input->anyKeyPressed()){
			if(input->wasKeyPressed(VK_BACK)){
				if(charStringForPlayerName.size() > 0)
					charStringForPlayerName.erase(charStringForPlayerName.end()-1);
			}
			else{
				if(input->wasKeyPressed(VK_SHIFT) || input->wasKeyPressed(VK_CAPITAL) || input->wasKeyPressed(ENTER_KEY) || input->wasKeyPressed(VK_SPACE)){
				}
				/*else if (input->wasKeyPressed(ENTER_KEY)){
				isPlayerDoneEnteringName = true;
				}*/
				else{
					if(charStringForPlayerName.size() < 10){ //allows for a name up to 10 characters long
						charStringForPlayerName += input->getCharIn();
						audio->playCue(TYPING);
					}
				}
			}
		}
		playerNameText << charStringForPlayerName;
		playerName->setFontColor(graphicsNS::BLUE);
		playerName->print(playerNameText.str(), 270, 450);
#pragma endregion

		break;
	case highScores:
		splashScreen1.draw();
		splashScreen2.draw();

		highscores.draw();

		int verticalOffset = 40;

		for(int i = 0; i < myHighScoresManager->names.size(); i++){
			highscoresNameText[i] << myHighScoresManager->names[i];
			highscoresScoreText[i] << myHighScoresManager->scores[i];
			highscoresMessage[i]->setFontColor(graphicsNS::WHITE);
			highscoresMessage[i]->print(highscoresNameText[i].str(), 100, (verticalOffset * i) + 120 );
			highscoresMessage[i]->print(highscoresScoreText[i].str(), 350, (verticalOffset * i) + 120 );
		}

		break;
	}

	graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void CollisionTypes::releaseAll()
{
	
	level1Texture.onLostDevice();
	level2Texture.onLostDevice();
	gameoverTexture.onLostDevice();
	youwinTexture.onLostDevice();
	splashTexture1.onLostDevice();
	splashTexture2.onLostDevice();
	menuTexture.onLostDevice();
	backTexture.onLostDevice();
	titleTexture.onLostDevice();
	highscoresTexture.onLostDevice();
	threeTexture.onLostDevice();
	twoTexture.onLostDevice();
	oneTexture.onLostDevice();
	goTexture.onLostDevice();

	Game::releaseAll();
	return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void CollisionTypes::resetAll()
{

	level1Texture.onResetDevice();
	level2Texture.onResetDevice();
	gameoverTexture.onResetDevice();
	youwinTexture.onResetDevice();
	splashTexture1.onResetDevice();
	splashTexture2.onResetDevice();
	menuTexture.onResetDevice();
	backTexture.onResetDevice();
	titleTexture.onResetDevice();
	highscoresTexture.onResetDevice();
	threeTexture.onResetDevice();;
	twoTexture.onResetDevice();
	oneTexture.onResetDevice();
	goTexture.onResetDevice();

	Game::resetAll();
	return;
}
